import 'dart:io';

class calculator {
  double sum(double a, double b) {
    return a + b;
  }

  double sub(double a, double b) {
    return a - b;
  }

  double mul(double a, double b) {
    return a * b;
  }

  double divide(double a, double b) {
    return a / b;
  }
}

void main(List<String> arguments) {
  calculator cal = calculator();
  print("1.sum");
  print("2.subtract");
  print("3.multipy");
  print("4.divide");
  print("5.exit");
  String? input = stdin.readLineSync();
  if (input == "5") {
    print("Exit program");
    exit(0);
  } else {
    print("Enter First number: ");
    String? firstInput = stdin.readLineSync();
    double a = double.parse(firstInput!);
    print("Enter Second number: ");
    String? secondInput = stdin.readLineSync();
    double b = double.parse(secondInput!);
    double result;
    switch (input) {
      case "1":
        result = cal.sum(a, b);
        print("$a + $b = $result");
        break;
      case "2":
        result = cal.sub(a, b);
        print("$a - $b = $result");
        break;
      case "3":
        result = cal.mul(a, b);
        print("$a * $b = $result");
        break;
      case "4":
        result = cal.divide(a, b);
        print("$a / $b = $result");
        break;
    }
  }
}
